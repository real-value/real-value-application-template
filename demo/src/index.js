import React, { useState, useCallback, useEffect } from 'react';
import { MemoryRouter as Router, Link, Switch, Route, Dialogue } from 'react-router-dom';
import { Navbar, Section, Sidebar,SidebarHeader, SidebarButton} from '@aurecon/design'
//import profilePic from './images/conor.png';
import { render} from 'react-dom';
import { RVGaugeControl } from 'real-value-react-gauge-component'

import Architecture from './Architecture.js'
import { EventPubSub} from 'real-value-event-pubsub'
import { Constants } from '../../src/index.js';

let streamAccess = EventPubSub()
let architecture = Architecture(streamAccess)

const UserPortal = props => {

    const [showSidebar, setShowSidebar] = useState(true);
  
    const handleHideSidebar = useCallback(() => {
      setShowSidebar(false);
    }, []);
  
    const handleToggleSidebar = useCallback(() => {
      setShowSidebar(!showSidebar);
    }, [showSidebar]);
  
    return (
      <div
        style={{
          width: '100vw',
          height: '100%',
          minHeight: '100vh',
          display: 'flex',
          flexDirection: 'column',
          alignContent: 'stretch',
          alignItems: 'stretch',
          justifyContent: 'stretch',
        }}
      >
            <Router>
            <Navbar
                title={<Link to="/">Style Library</Link>}
                
                accountActions={<i className="fas fa-search"></i>}
                sidebarCallback={handleToggleSidebar}
                isDark={props.isDark}
            >
                <Link to="/one">Page One</Link>
                <Link to="/two">Page Two</Link>
                <Link to="/three">Run Model</Link>
            </Navbar>
            <div style={{ flex: '1 1', display: 'flex', flexDirection: 'row' }}>
                {showSidebar && (
                <Sidebar footerCallback={handleHideSidebar} isDark={props.isDark}>
                    <SidebarButton
                    label="Item One"
                    href="/itemOne"
                    isDark={props.isDark}
                    />
                    <SidebarButton
                    label="Item Two"
                    isOpen={true}
                    href="/itemTwo"
                    isDark={props.isDark}
                    >
                    <SidebarButton
                        label="Nested Item One"
                        isSecondary={true}
                        href="/itemTwo/one"
                        isDark={props.isDark}
                    />
                    <SidebarButton
                        label="Nested Item Two"
                        isSecondary={true}
                        href="/itemTwo/two"
                        isDark={props.isDark}
                    />
                    </SidebarButton>
                </Sidebar>
                )}
                <div style={{ padding: '10px 30px', width: '100%' }}>
                <Switch>
                    <Route path="/one">
                    <div>
                        <h1>Page One</h1>
                        Some Text
                    </div>
                    </Route>
                    <Route path="/two">
                    <div>
                        <h1>Page Two</h1>
                        Some Text
                    </div>
                    </Route>
                    <Route path="/three">
                    <div>
                        <h1>Run Model</h1>
                        <RVGaugeControl name="Production" initial="1" streamAccess={streamAccess} channel={Constants.PERFORMANCE} control={architecture.controlProducer} />
                    </div>
                    </Route>
    
                    <Route path="/itemOne">
                    <h1>Sidebar Item One</h1>
                    </Route>
                    <Route path="/itemTwo/one">
                    <h1>Nested Item One</h1>
                    </Route>
                    <Route path="/itemTwo/two">
                    <h1>Nested Item Two</h1>
                    </Route>
                    <Route path="/itemTwo">
                    <h1>Sidebar Item Two</h1>
                    </Route>
    
                    <Route path="/">
                    
                    </Route>
                </Switch>
                </div>
            </div>
            </Router>
      </div>
    );
  };


render(<UserPortal />, document.getElementById("root"));

