import React,{ useEffect } from 'react';
import { Model} from 'real-value-lang'
import {produceData,produceControl,produceDisplay} from '../../src'

const Architecture = (streamAccess)=>{

    let model = Model()

    let citec = model.from([{production: 1},{production: 3},{production: 5}]).delay(5000)
    let corvus = model.from([{target: 100,measure: 'stockpile1',time: 2}])
    let wenco = model.from([{status: 'off',asset: 'digger-1', time: 4}])

    let architecture={
        model,
        source_target: corvus, 
        source_production: citec,
        source_asset: wenco,
    }

    architecture = produceData(architecture)
    architecture = produceControl(architecture)
    architecture = produceDisplay(streamAccess,architecture)

    architecture.controlStream.log()

    architecture.model.run()

    return architecture
}

export default Architecture