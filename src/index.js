import React from 'react';
import {RVGaugeControl} from 'real-value-react-gauge-component'

export const Constants = {
    PERFORMANCE: "performance"
}

export function produceData(architecture={}){
    const {
        model,

        source_target,
        source_production,
        source_asset,
        } = architecture

    return {
        ...architecture,
        stream_asset: source_asset.filter(x=>x).map(x=>x),
        stream_production: source_production.filter(x=>x).map(x=>x),
        stream_target: source_target.filter(x=>x).map(x=>x),
    }
}

export function produceControl(architecture={}){
    const {
        model
        } = architecture

    //Input Control Events
    let { stream: controlStream, producer: controlProducer } = model.fromCallback()

    return {
        ...architecture,
        controlStream,
        controlProducer
    }
}

export function produceDisplay(streamAccess, architecture={}){
    const presentationState = {}

    const {
        stream_asset,
        stream_production,
        stream_target,

        controlStream,
        controlProducer
    } = architecture

    let performanceStream = stream_production    
        .join(stream_target)
        .join(stream_asset)
        .map(x=>x.production)
        
    architecture = {
        ...architecture,
    }
    
    performanceStream.log().tap(x=>streamAccess.dispatch(Constants.PERFORMANCE,x))

    return {
        ...architecture,
        performanceStream,
        presentationState
    }
}

