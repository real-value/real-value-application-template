import { describe } from 'riteway'
import Debug from 'debug'
import {Model}  from 'real-value-lang'

import {produceData,produceControl,produceDisplay} from '../src'

import EventPubSub from 'real-value-event-pubsub'

let eventPubSub = EventPubSub()

function provisionSystem(architecture){
  architecture = produceData(architecture)
  architecture = produceControl(architecture)
  return produceDisplay(eventPubSub,architecture)
}

describe('Domain', async (assert) => {
  let productionCount=0
  let model = Model()

  //Test Data Source Stream
  let citec = model.from([{production: 1,measure: 'stockpile1',time: 0}])
  let corvus = model.from([{target: 100,measure: 'stockpile1',time: 2}])
  let wenco = model.from([{status: 'off',asset: 'digger-1', time: 4}])

  let architecture = {
    model,
    source_production: citec,
    source_target: corvus,
    source_asset: wenco,
  }

  architecture = provisionSystem(architecture) //under test

  architecture.performanceStream.tap(x=>{
    productionCount += 1
  })
  
  model.run(()=>{
    assert({
      given: 'an architecture for idemitsu',
      should: 'update presentation appropriately',
      actual: `${productionCount}`, 
      expected: '1'
    })
  })
})

 